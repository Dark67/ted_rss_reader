package com.darklab.TED_RSS_Reader;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by aleksandrlihovidov on 29.04.15.
 */
public class RSSItemLab {
    private ArrayList<RSSItem> mRSSItems;

    private static RSSItemLab sRSSItemLab;
    private Context mAppContext;

    private RSSItemLab(Context appContext) {
        mAppContext = appContext;
        mRSSItems = new ArrayList<RSSItem>();
    }

    public static RSSItemLab getInstance(Context context) {
        if (sRSSItemLab == null) {
            sRSSItemLab = new RSSItemLab(context.getApplicationContext());
        }
        return sRSSItemLab;
    }

    public ArrayList<RSSItem> getRSSItems() {
        return mRSSItems;
    }

    public RSSItem getRSSItem(int position) {
        return mRSSItems.get(position);
    }

    public void add(RSSItem item) {
        mRSSItems.add(item);
    }
}
