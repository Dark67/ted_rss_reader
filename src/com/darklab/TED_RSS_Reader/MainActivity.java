package com.darklab.TED_RSS_Reader;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;

public class MainActivity extends SingleFragmentActivity implements MainFragment.Callbacks{

    @Override
    protected Fragment createFragment() {
        return new MainFragment();
    }

    @Override
    public void onRSSItemSelected(int position) {
        if (findViewById(R.id.detailFragmentContainer) == null) {
            Intent i = new Intent(this, MediaPlayerActivity.class);
            i.putExtra(MediaPlayerFragment.INDEX, position);
            startActivity(i);
        } else {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment oldDetail = fm.findFragmentById(R.id.detailFragmentContainer);
            Fragment newDetail = MediaPlayerFragment.newInstance(position);
            if (oldDetail != null) {
                ft.remove(oldDetail);
            }
            ft.add(R.id.detailFragmentContainer, newDetail);
            ft.commit();
        }
    }
}
