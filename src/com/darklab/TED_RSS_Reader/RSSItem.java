package com.darklab.TED_RSS_Reader;

/**
 * Created by aleksandrlihovidov on 29.04.15.
 */
public class RSSItem {
    private String mTitle;
    private String mLink;
    private String mDescription;
    private String mUrl;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }
}
