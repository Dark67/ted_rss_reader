package com.darklab.TED_RSS_Reader;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;

/**
 * Created by aleksandrlihovidov on 30.04.15.
 */
public class MediaPlayerFragment extends Fragment {
    public static final String INDEX = "index";
    private RSSItem mRSSItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int n = getArguments().getInt(INDEX);
        mRSSItem = RSSItemLab.getInstance(getActivity()).getRSSItem(n);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle(mRSSItem.getTitle());
        View v = inflater.inflate(R.layout.player_fragment, container, false);

        RSSItemVideoView videoView = (RSSItemVideoView) v.findViewById(R.id.videoView);
        MediaController mc = new MediaController(getActivity());
        videoView.setMediaController(mc);
        videoView.setVideoURI(Uri.parse(mRSSItem.getUrl()));
        videoView.requestFocus();
        videoView.start();

        return v;
    }

    public static MediaPlayerFragment newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt(INDEX, position);
        MediaPlayerFragment fragment = new MediaPlayerFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
