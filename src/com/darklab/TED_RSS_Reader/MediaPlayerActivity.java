package com.darklab.TED_RSS_Reader;

import android.app.Fragment;

/**
 * Created by aleksandrlihovidov on 29.04.15.
 */
public class MediaPlayerActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return MediaPlayerFragment.newInstance(getIntent().getExtras().getInt(MediaPlayerFragment.INDEX));
    }
}
