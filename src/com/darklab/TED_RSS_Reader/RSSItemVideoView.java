package com.darklab.TED_RSS_Reader;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * Created by aleksandrlihovidov on 30.04.15.
 */
public class RSSItemVideoView extends VideoView {
    public RSSItemVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int size = Math.min(widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(size, size);
    }
}
