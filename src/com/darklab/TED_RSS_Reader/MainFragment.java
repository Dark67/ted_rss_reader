package com.darklab.TED_RSS_Reader;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by aleksandrlihovidov on 30.04.15.
 */
public class MainFragment extends ListFragment {

    private String finalUrl="http://www.ted.com/themes/rss/id/6";
    private ArrayList<RSSItem> mRSSItems;
    private Callbacks mCallbacks;
    private MyDialogFragment dialogFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRSSItems = RSSItemLab.getInstance(getActivity()).getRSSItems();
        downloadItems();

        RSSListAdapter adapter = new RSSListAdapter(mRSSItems);
        setListAdapter(adapter);

        setRetainInstance(true);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mCallbacks.onRSSItemSelected(position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh_list:
                downloadItems();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void downloadItems() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new HandleAsyncTask().execute(finalUrl);
        } else {
            dialogFragment = new MyDialogFragment();
            dialogFragment.show(getFragmentManager(), "DialogFragment");
        }
    }

    private class HandleAsyncTask extends AsyncTask<String, Void, Void> {
        private InputStream stream;
        private HandleXML obj;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mRSSItems.clear();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();

                stream = conn.getInputStream();
                XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
                XmlPullParser myParser = xmlFactoryObject.newPullParser();
                myParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                myParser.setInput(stream, null);

                obj = new HandleXML(mRSSItems);
                obj.parseXMLDocumentWith(myParser);
            } catch (Exception e) {
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }finally {
                try {
                    if (stream != null) {
                        stream.close();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), String.format(getActivity().getString(R.string.number_of_items), mRSSItems.size()), Toast.LENGTH_SHORT)
                    .show();

            ((ArrayAdapter<RSSItem>)getListAdapter()).notifyDataSetChanged();
        }
    }

    private class RSSListAdapter extends ArrayAdapter<RSSItem> {

        public RSSListAdapter(ArrayList<RSSItem> items) {
            super(getActivity(), 0, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item, null);
            }

            RSSItem item = getItem(position);

            TextView tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            tvTitle.setText(item.getTitle());

            TextView tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
            tvDescription.setText(item.getDescription());

            return convertView;
        }
    }

    @SuppressLint("ValidFragment")
    public class MyDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getActivity().getString(R.string.connection_error))
                    .setPositiveButton(getActivity().getString(R.string.check_and_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            downloadItems();
                        }
                    })
                    .setNegativeButton(getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.i("MainFragment", "Cancel");
                        }
                    });

            return builder.create();
        }
    }

    public interface Callbacks {
        void onRSSItemSelected(int position);
    }
}
